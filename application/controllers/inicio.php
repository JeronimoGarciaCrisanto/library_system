<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inicio extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    
        public function __construct(){
            parent::__construct();
            $this->load->model('usuario');
        }

	public function index(){
            $this->load->view('inicio_login');
	}
	
	public function autentifica(){
            $user=$_POST['usuario'];
            $pass = $_POST['password'];
            $usuarios=$this->usuario->valida_user();
            
            $data['usuarios'] = $usuarios;
            $data['usuario']=$user;
            $data['password']=$pass;
            
            
            if($usuarios['nombre']==$user)
            $this->load->view('inicio_bienvenida', $data);
            else redirect('');

           
        }
  
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
