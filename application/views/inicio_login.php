<!DOCTYPE html>
<html lang="es"> 
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Login</title>
  <link rel="stylesheet" href="css/style_login.css">
  <!--[if lt IE 9]><script src="js/login.js"></script><![endif]-->
</head>
<body>
    
    
<?php
$atribform = array('class' => 'login',
                'id' => 'login');

  echo form_open('inicio/autentifica/', $atribform);
  echo "\n";
  echo form_fieldset("Login");
  echo "\n";
  
  $attriblabel1 = array(
            'class' => 'mi_clase',
            'for' => 'login',
            );
echo form_label('Usuario', 'usuario', $attriblabel1);  
  $atribuser = array(
            'name'=> 'usuario',
            'id'=> 'usuario',
            'value' => '-----',);
  echo form_input($atribuser);
  
  echo "\n";
  
   $attriblabel2 = array(
            'class' => 'password',
            'for' => 'password',
            );
   echo form_label('Password', 'password', $attriblabel2);
  
  $attribpass = array(
            'name'=> 'password',
            'id'=> 'password',
            'value' => '0000',);
  echo form_password($attribpass);

  
  ?>
  <p class="login-submit">  
  <?php
  $attriblogin = array(
            'class' => 'login-button',
            'value'=>'Login',
            'type'=>'submit',
            );
  echo form_button($attriblogin);
  ?>
   </p>
  <?php 
  echo form_fieldset_close();
  echo "\n";
  echo form_close();
?>
 
   
   <section class="forgot">
        <p class="about-links">
            <a href="#">¿Olvidaste tu contraseña?</a>
        </p>
   </section>
    
</body>
</html>